package main;

import user.input.UserInput;

/**
 * <h1>Fibonacci numbers</h1>
 * Program, that works with Fibonacci numbers.
 * It can print odd numbers for start to end, print even numbers from end to start,
 * from the biggest even and biggest odd numbers it can build a sequence of Fibonacci numbers and
 * print the percentage of odd and even Fibonacci numbers.
 *
 * @author Ivan Ostapchuk ivan.ostapchuk.27@gmail.com
 * @version 1.0
 */
public class Start {
  public static void main(String[] args) {
    UserInput.manageInputs();
  }
}
