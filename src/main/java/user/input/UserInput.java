package user.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import mathematics.Interval;

/**
 * A utility class that handles all user inputs and
 * communicates with maths side of the project.
 *
 * @author Ivan Ostapchuk ivan.ostapchuk.27@gmail.com
 * @version 1.0
 */

public final class UserInput {
  private UserInput() {

  }

  // initializing to avoid nullPointer bugs
  private static int[] intervalArray = {0, 1};

  /**
   * Method, that operates all other methods in class.
   * Starting point for using this class as all other methods are private.
   */
  public static void manageInputs() {
    System.out.println("Hello, user, first, enter the interval numbers:");
    intervalArray = getInterval();
    manageMenu();
  }

  private static int[] getInterval() {
    int[] interval = new int[2];
    interval[0] = inputNumber("Enter the first number: ");
    System.out.println();
    /**
     * Here and later this type of for cycle is used to be able to use
     * the program a lot of times with or without input mistakes.
     * For breaks only when user inputs plausible data.
     */
    for (; ; ) {
      interval[1] = inputNumber("Enter the second number: ");
      if (interval[1] < interval[0]) {
        System.out.println("Second number of interval "
            + "has to be bigger than the first one! Try again.");
        continue;
      } else {
        break;
      }
    }
    System.out.println();
    return interval;
  }

  private static int inputNumber(String firstString) {
    for (; ; ) {
      System.out.println(firstString);
      try {
        BufferedReader bufferedReader = new BufferedReader(
            new InputStreamReader(System.in));
        String inputString = bufferedReader.readLine();
        return Integer.parseInt(inputString);
      } catch (IOException exception) {
        System.out.println(exception.getMessage());
      } catch (NumberFormatException notANumber) {
        System.out.println("Oops, looks like you did not type in a number, try again!");
        continue;
      }
    }
  }

  private static void printMenu() {
    System.out.println("\nMenu:");
    System.out.println("1. Print odd numbers from the start of the interval to the end.");
    System.out.println("2. Print even numbers from the end of the interval to the start.");
    System.out.println("3. Print the sum of odd numbers.");
    System.out.println("4. Print the sum of even numbers.");
    System.out.println("5. Build Fibonacci numbers sequence.");
    System.out.println("First number of the set will be "
        + "the biggest odd number in the interval \nand the second number will be "
        + "the biggest even number in the interval.");
    System.out.println("6. Print the percentage of odd Fibonacci numbers "
        + "\n(will also do the 5th option to create Fibonacci numbers set).");
    System.out.println("7. Print the percentage of even Fibonacci numbers "
        + "\n(will also do the 5th option to create Fibonacci numbers set).");
    System.out.println("8. Change interval.");
    System.out.println("0. Exit.");
  }

  private static void manageMenu() {
    // Access to the methods will be obtained through this object
    Interval interval = new Interval(intervalArray[0], intervalArray[1]);
    for (; ; ) {
      printMenu();
      int menuChoice = inputNumber("Now choose what do you want to do:");
      if (menuChoice == 1) {
        interval.printOddNumbersFromStartToEnd();
      } else if (menuChoice == 2) {
        interval.printEvenNumbersFromEndToStart();
      } else if (menuChoice == 3) {
        interval.printOddNumbersSum();
      } else if (menuChoice == 4) {
        interval.printEvenNumbersSum();
      } else if (menuChoice == 5) {
        interval.printFibonacciNumbers(
            inputNumber("Please enter the size of the Fibonacci numbers set: "));
      } else if (menuChoice == 6) {
        String firstString = "Please enter the size of the Fibonacci numbers set:";
        int sizeOfSet = inputNumber(firstString);
        System.out.println("Percentage of odd numbers: ");
        System.out.print(interval.getPercentageOfOdds(sizeOfSet) + "%");
      } else if (menuChoice == 7) {
        String firstString = "Please enter the size of the Fibonacci numbers set: ";
        int sizeOfSet = inputNumber(firstString);
        System.out.println("Percentage of even numbers: ");
        System.out.print(interval.getPercentageOfEvens(sizeOfSet) + "%");
      } else if (menuChoice == 8) {
        intervalArray = getInterval();
      } else if (menuChoice == 0) {
        System.out.println("Bye!");
        break;
      } else {
        System.out.println("You have chose an option that is not on the menu! Try again.");
      }
    }
  }
}
