package mathematics;

import java.util.Arrays;

public class Interval {
  private final int smallerBorder;
  private final int biggerBorder;

  public Interval(int smallerNumber, int biggerNumber) {
    this.smallerBorder = smallerNumber;
    this.biggerBorder = biggerNumber;
  }

  /**
   * Printing a sequence of Fibonacci numbers.
   * @param sizeOfSet - how many numbers will be printed
   */
  public void printFibonacciNumbers(int sizeOfSet) {
    System.out.println("Set of Fibonacci numbers will start with numbers "
        + returnBiggestOdd() + " and " + returnBiggestEven()
        + " and will have " + sizeOfSet + " numbers.");
    int[] fibonacciNumbers = returnFibonacciNumbers(sizeOfSet);
    for (int i : fibonacciNumbers) {
      System.out.println(i);
    }
  }

  public double getPercentageOfOdds(int sizeOfSet) {
    int[] fibonacciNumbers = returnFibonacciNumbers(sizeOfSet);
    double percentage = (double)
        getNumberOfFibonacciOdds(fibonacciNumbers) / fibonacciNumbers.length;
    percentage *= 100;
    return percentage;
  }

  public double getPercentageOfEvens(int sizeOfSet) {
    int[] fibonacciNumbers = returnFibonacciNumbers(sizeOfSet);
    double percentage = (double)
        getNumberOfFibonacciEvens(fibonacciNumbers) / fibonacciNumbers.length;
    percentage *= 100;
    return percentage;
  }

  public void printEvenNumbersFromEndToStart() {
    System.out.println("Even numbers from " + this.biggerBorder + " to " + this.smallerBorder);
    int[] evenNums = returnEvenArray();
    for (int i = evenNums.length - 1; i >= 0; i--) {
      System.out.println(evenNums[i]);
    }
  }

  public void printOddNumbersFromStartToEnd() {
    System.out.println("Odd numbers from " + this.smallerBorder + " to " + this.biggerBorder);
    int[] oddNums = returnOddArray();
    for (int i : oddNums) {
      System.out.println(i);
    }
  }

  public void printEvenNumbersSum() {
    int evenNumbersSum = 0;
    System.out.println("Sum of all of the even numbers: ");
    int[] evenNums = returnEvenArray();
    for (int i : evenNums) {
      evenNumbersSum += i;
    }
    System.out.print(evenNumbersSum + "\n");
  }

  public void printOddNumbersSum() {
    int evenNumbersSum = 0;
    System.out.println("Sum of all of the odd numbers: ");
    int[] oddNums = returnOddArray();
    for (int i : oddNums) {
      evenNumbersSum += i;
    }
    System.out.print(evenNumbersSum + "\n");
  }

  private int[] returnEvenArray() {
    int[] evenNumbers = new int[returnNumberOfEvens()];
    int evenArrayPosition = 0;
    for (int i = this.smallerBorder; i <= this.biggerBorder; i++) {
      if ((i % 2) == 0) {
        evenNumbers[evenArrayPosition] = i;
        evenArrayPosition++;
      }
    }
    return evenNumbers;
  }

  private int[] returnOddArray() {
    int[] oddNumbers = new int[returnNumberOfOdds()];
    int oddArrayPosition = 0;
    for (int i = this.smallerBorder; i <= this.biggerBorder; i++) {
      if ((i % 2) != 0) {
        oddNumbers[oddArrayPosition] = i;
        oddArrayPosition++;
      }
    }
    return oddNumbers;
  }

  private int returnBiggestEven() {
    int[] evenNumbers = returnEvenArray();
    return Arrays.stream(evenNumbers).max().getAsInt();
  }

  private int returnBiggestOdd() {
    int[] oddNumbers = returnOddArray();
    return Arrays.stream(oddNumbers).max().getAsInt();
  }

  private int returnNumberOfEvens() {
    int numberOfEvens = 0;
    for (int i = this.smallerBorder; i <= this.biggerBorder; i++) {
      if ((i % 2) == 0) {
        numberOfEvens++;
      }
    }
    return numberOfEvens;
  }

  private int returnNumberOfOdds() {
    int numberOfOdds = 0;
    for (int i = this.smallerBorder; i <= this.biggerBorder; i++) {
      if ((i % 2) != 0) {
        numberOfOdds++;
      }
    }
    return numberOfOdds;
  }

  private int[] returnFibonacciNumbers(int sizeOfSet) {
    int[] fibonacciNumbers = new int[sizeOfSet];
    int fibonacci1 = returnBiggestOdd();
    int fibonacci2 = returnBiggestEven();
    fibonacciNumbers[0] = fibonacci1;
    fibonacciNumbers[1] = fibonacci2;
    for (int i = 2; i < fibonacciNumbers.length; i++) {
      fibonacciNumbers[i] = fibonacci1 + fibonacci2;
      fibonacci1 = fibonacci2;
      fibonacci2 = fibonacciNumbers[i];
    }
    return fibonacciNumbers;
  }

  private int getNumberOfFibonacciOdds(int[] fibonacciNumbers) {
    int numberOfOdds = 0;
    for (int i : fibonacciNumbers) {
      if ((i % 2) == 1) {
        numberOfOdds++;
      }
    }
    return numberOfOdds;
  }

  private int getNumberOfFibonacciEvens(int[] fibonacciNumbers) {
    int numberOfEvens = 0;
    for (int i : fibonacciNumbers) {
      if ((i % 2) == 0) {
        numberOfEvens++;
      }
    }
    return numberOfEvens;
  }
}
